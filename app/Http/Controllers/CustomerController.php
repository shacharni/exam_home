<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\User;
use\Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers=Customer::all();
        $id = Auth::id();
        return view("customers.index",['customers'=>$customers,'id' =>$id]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("customers.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer=new Customer();
        $id=Auth::id();
        $boss = DB::table('users')->where('id',$id)->first();
        $username = $boss->name;
        //כל יוזר מקושר לקסטומר שלו 
        $customer->name=$request->name;
        $customer->email=$request->email;
        $customer->phone=$request->phone;
        $customer->username = $username;
        $customer->user_id=$id;
        $customer->save();
        return redirect('customers');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        $id = Auth::id();
        
            if (Gate::denies('manager')and ($customer->user_id != $id)) {
                abort(403,"Sorry you are not allowed to edit this customer..");
            } 
        return view('customers.edit', ['customer' => $customer]);
 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $customer=Customer::find($id);
        $customer->update($request->all());
        return redirect ('customers');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
        }

        $customer=Customer::find($id);
        $customer->delete();
        return redirect ('customers');

    }
   // public function change_status($id,$status,Request $request)
   // {
     //   if (Gate::denies('admin')) {
     //       abort(403,"Sorry you are not allowed to change status..");
      //  } 
      //  $customer = Customer::find($id);
      //  $customer->status = 1; 
      //  $customer->update($request -> all());
      //  return redirect('customers');
    }
   

