@extends('layouts.app')

@section('content')

<!DOCTYPE html>
<html>
<h1> this is a customer list</h1>
<a href="{{route('customers.create')}}">Create a new customer</a>
    <head>
    </head>
    <body>
    <table style="width:80%">
  <tr>
  <th></th>
    <th>name</th>
    <th>email</th>
    <th>phone</th>
    <th>username</th>
  </tr>
  @foreach($customers as $customer)
  @if($id==($customer->user_id))
  <tr style="font-weight:bold">
  @else
  <tr>
  @endif
 <td>
  @if ($customer->status)
  @can('manager')   <button  class="btn btn-link" id ="" value="1"></button>@endcan
          
        <td style="color: green;">{{$customer->name}}</td>
        <td style="color: green;">{{$customer->email}}</td>
        <td style="color: green;">{{$customer->phone}}</td>
        <td style="color: green;">{{$customer->username}}</td>
        
       @else
       @can('manager')  <button class="btn btn-link" id ="{{$customer->id}}" value="0"> deal closed</button>@endcan
          
  </td>  
  <td >{{$customer->name}}</td>
  <td>{{$customer->email}}</td>
  <td>{{$customer->phone}}</td>
  <td>{{$customer->username}}</td>
  @endif 
 
  <td> <a href="{{route('customers.edit',$customer->id)}}"> edit</a></td>
 @can('manager')<td><br><form method='post' action="{{action('CustomerController@destroy',$customer->id)}}">
    @csrf
    @method('DELETE')

    <div class = "form-group">
        <input type="submit"  class="btn btn-link"  name="submit" value="delete">
    </div>
</form>
</td>@endcan
@can('salesrep')<td>delete</td>@endcan

@endforeach
   </body>
   <script>
$(document).ready(function(){
           $("button").click(function(event){
            console.log(event.target.id)
               $.ajax({
                   url:  "{{url('customers')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type:  'put',
                   contentType: 'application/json',
                   data: JSON.stringify({'status':(event.target.value-1)*-1, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                      console.log( errorThrown );
                   }
               });               
              location.reload();
           });
       });
</script>

</html>

@endsection

