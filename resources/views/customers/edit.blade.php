@extends('layouts.app')

@section('content')

<h1>Update a customer</h1>
<form method='post' action="{{action('CustomerController@update',$customer->id)}}">
    @csrf
    @method('PATCH')

    <div class="form-group">
        <label for ="name"> name to update</label>
        <input type="text" class= "form-control" name="name" value="{{$customer->name}}">
        <label for ="email"> email to update</label>
        <input type="text" class= "form-control" name="email" value="{{$customer->email}}">
        <label for ="phone">phone to update</label>
        <input type="text" class= "form-control" name="phone" value="{{$customer->phone}}">
    </div>
    
    <div class = "form-group">
        <input type="submit" class="form-controll" name="submit" value="update">
    </div>
</form>
@endsection